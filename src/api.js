const express = require('express');
var bodyParser = require('body-parser');
var api = express.Router();
const client = require('mongodb').MongoClient;
var path = require('path');

api.use(bodyParser.urlencoded({
  extended: true
}));
api.use(bodyParser.json());

var url = 'mongodb://localhost:27017/barmApp';
var db = null;

client.connect(url, (err, database) => {
  if (!err) {
    console.log('connected to database');
    db = database;
  }
});

api.post('/:codigo/invitados', (req, res) => {
  db.collection(req.body.codigo).find({}).toArray((err, invitados) => {
    if (!err) {
      res.json({
        mensaje: 'ok',
        invitados: invitados
      });
    }
  });
});

api.post('/crearGrupo', (req, res) => {
  db.collection(req.body.codigo).insert({
    nombre: 'host'
  }, (err) => {
    if (!err) {
      res.json({
        mensaje: `ok`,
        alert: `Se creo el grupo ${req.body.codigo} con el Host`,
        grupo: req.body.codigo,
        miembro: 'host'
      });
    } else {
      res.json({
        mensaje: 'no se pudo crear el grupo'
      });
    }
  });
});

api.post('/:codigo', (req, res) => {
  var nuevoUsuario = {
    nombre: req.body.nombre
  };
  db.collection(req.body.codigo).insert(nuevoUsuario, (err) => {
    if (!err) {
      res.json({
        mensaje: 'ingresado'
      });
    } else {
      res.json({
        mensaje: 'no se pudo ingresar el mensaje'
      });
    }
  });
});

api.post('/:codigo/resultados', (req, res) => {
  var resultado = {
    nombre: req.body.invitado,
    presupuesto: req.body.presu,
    isComida: req.body.isFood,
    tipoComida: req.body.typeFood,
    musica: req.body.music,
    espacios: req.body.espacio,
    zona: req.body.zona,
    ambiente: req.body.ambient
  };
  db.collection(req.body.codigo).findOneAndUpdate({ nombre: req.body.invitado }, resultado, (err) => {
      if (!err) {
        res.json({
          mensaje: 'resultados ingresados'
        });
      } else {
        res.json({
          mensaje: 'error al registrar datos'
        });
      }
    });
});



module.exports = api;
