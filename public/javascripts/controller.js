var controller = function controller(view) {

  // clear DB in mongoshell
  // use [database];
  // db.dropDatabase();
  //
  // fetch(`${location.origin}/api/grupos`).then((res) => res.json()).then((res) => {
  //   if (res.mensaje == 'ok') {
  //     console.log(res.grupos);
  //   }
  // });

  view.crearGrupo = function crearGrupo() {
    var codigo = `A${Math.floor((Math.random() * 9))}${Math.floor((Math.random() * 9) + 1)}${Math.floor((Math.random() * 9) + 1)}${Math.floor((Math.random() * 9) + 1)}${Math.floor((Math.random() * 9) + 1)}${Math.floor((Math.random() * 9) + 1)}${Math.floor((Math.random() * 9) + 1)}`;
    var param = new URLSearchParams();
    param.set('codigo', codigo);
    fetch(`${location.origin}/api/crearGrupo`, {
        method: 'POST',
        body: param
      })
      .then((res) => res.json())
      .then((res) => {
        if (res.mensaje == 'ok') {
          console.log(res.grupo);
          view.miembro = res.miembro;
          view.miGrupo = codigo;
          view.render('crear');
          alert(res.alert);
        }
      });
  };

  view.onUnir = function onUnir(nombre, codigo) {
    var params = new URLSearchParams();
    params.set('nombre', nombre);
    params.set('codigo', codigo);
    fetch(`${location.origin}/api/${codigo}`, {
        method: 'POST',
        body: params
      })
      .then((res) => res.json())
      .then((res) => {
        if (res.mensaje == 'ingresado') {
          if (view.miembro != 'host' && view.miembro == '') {
            view.miembro = nombre;
            console.log(view.miembro);
          } else {
            console.log('no se pudo ingresar usuario');
          }
          view.miGrupo = codigo;
          console.log(nombre + ' entraste al grupo ' + codigo);
          setInterval((e) => {
            view.onActualizar(codigo);
          }, 1000);
        }
      });
  };

  view.onActualizar = function onActualizar(codigo) {
    var param = new URLSearchParams();
    param.set('codigo', codigo);
    fetch(`${location.origin}/api/${codigo}/invitados`, {
        method: 'POST',
        body: param
      })
      .then((res) => res.json())
      .then((res) => {
        if (res.mensaje == 'ok') {
          view.invitados = res.invitados;
          view.render('grupo');
        }
      });
  };

  view.onResults = function onResults(invitado, grupo, presupuesto, isFood, food, music, spaces, zona, ambient) {
    var params = new URLSearchParams();
    params.set('codigo', grupo);
    params.set('invitado', invitado);
    params.set('presu', presupuesto);
    params.set('isFood', isFood);
    params.set('typeFood', food);
    params.set('music', music);
    params.set('espacio', spaces);
    params.set('zona', zona);
    params.set('ambiente', ambient);
    console.log(grupo);
    fetch(`${location.origin}/api/${grupo}/resultados`, {
        method: 'POST',
        body: params
      })
      .then((res) => res.json())
      .then((res) => {
        if (res.mensaje == 'resultados ingresados') {
          console.log('habemus resulados');
        } else {
          console.log('cries');
        }
      });
  };


  view.render();
}

controller(view);
