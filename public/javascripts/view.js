var view = {
  miGrupo: '',
  invitados: null,
  miembro: '',
  getLogin: function getLogin() {
    var formulario = document.createElement('div');
    return formulario;
  },

  getHome: function getHome() {
    var home = document.createElement('div');
    home.setAttribute('id', 'home');
    home.innerHTML = `
    <img src="" />
    <p>Esta aplicación web permitirá a un grupo de usuarios
tener un ábanico de opciones a la hora de escoger
un bar afin a sus necesidades</p>
<p>Únete a un grupo o puedes crearlo con tus
amigos para encontrar una alternativa ideal</p>
    <button id="crear">Crear</button>
    <button id="unir">Unirse</button>
    `;
    var that = this;
    home.querySelector('#crear').addEventListener('click', (e) => {
      e.preventDefault();
      this.crearGrupo();
    });
    home.querySelector('#unir').addEventListener('click', (e) => {
      e.preventDefault();
      this.render('unir');
      console.log('unir');
    });
    return home;
  },

  getCrearGrupo: function getCrearGrupo() {
    console.log(this.miGrupo, this.miembro);
    var crear = document.createElement('div');
    crear.setAttribute('id', 'crearGrupo');
    crear.innerHTML = `
    <p>Comparte este código con tus amigos para
    unirse al grupo y asi empezar a recomendar</p>
    <h1>${this.miGrupo}</h1>
    <button>Comenzar</button>
    `;
    var that = this;
    crear.querySelector('button').addEventListener('click', (e) => {
      e.preventDefault();
      that.render('formul');
    });
    return crear;
  },

  getUnirseGrupo: function getUnirseGrupo(post) {
    var unir = document.createElement('div');
    unir.setAttribute('id', 'unirseGrupo');
    unir.innerHTML = `
    <p>Ingresa el código de un grupo para
    ser parte y comenzar a recomendar</p>
    <form>
    <input type="text" name="nombre" placeholder="Tu nombre" /> <br>
    <input type="text" name="codigo" placeholder="Codigo del grupo" /> <br>
    <input type="submit" value="Empezar" />
    </form>
    `;
    var that = this;
    unir.querySelector('form').addEventListener('submit', function(e) {
      e.preventDefault();
      that.onUnir(e.target.nombre.value, e.target.codigo.value);
    });
    return unir;
  },

  getMiGrupo: function getMiGrupo() {
    //console.log(this.invitados);
    var miGrupo = document.createElement('div');
    miGrupo.setAttribute('id', 'miGrupo');
    miGrupo.innerHTML = `
    <p> Esperando a tus invitados, puedes comenzar cuando ya veas que estan todos </p>
    <button> Comenzar </button>
    `;
    var invitados = this.invitados;
    var that = this;
    var lista = document.createElement('ul');
    invitados.forEach(function(invitado) {
      var invi = that.getInvitado(invitado);
      lista.appendChild(invi);
    });
    miGrupo.appendChild(lista);
    miGrupo.querySelector('button').addEventListener('click', (e) => {
      e.preventDefault();
      that.render('formul');
    });
    return miGrupo;
  },

  getInvitado: function getInvitado(invitado) {
    var invit = document.createElement('li');
    invit.innerHTML = `
    <h4>${invitado.nombre}</h4>
    `;
    return invit;
  },

  getAllForm: function getAllForm() {
    var formulario = document.createElement('div');
    formulario.setAttribute('id', 'fomulario');
    formulario.innerHTML = `
    <form>
    <h3>¿Cuánto dinero estarías dispuesto a gastar en un bar?</h3>
    <input type='radio' name='presupuesto' value='20000'> 10.000 - 20.000 <br>
    <input type='radio' name='presupuesto' value='30000'> 20.000 - 30.000 <br>
    <input type='radio' name='presupuesto' value='40000'> 30.000 - 40.000 <br>
    <input type='radio' name='presupuesto' value='50000'> 40.000 - 50.000 <br>
    <input type='radio' name='presupuesto' value='more'> 50.000 o más... <br>
    <h3>¿Te gusta tener la opcion de comida en un bar o solo beber?</h3>
    <input type='radio' name='isFood' value='yes'> Me gusta que haya comida <br>
    <input type='radio' name='isFood' value='no'> No, solo voy para beber <br>
    <h3>¿Qué opciones te gustaria tener de comida?</h3>
    <input type='checkbox' name='food' value='Fast'> Rápida <br>
    <input type='checkbox' name='food' value='Gourmet'> Gourmet <br>
    <input type='checkbox' name='food' value='Fusion'> Fusion <br>
    <input type='checkbox' name='food' value='Regional'> Regional <br>
    <h3>¿Qué tipo de musica escuchas?</h3>
    <input type='checkbox' name='music' value='Rock'> Rock <br>
    <input type='checkbox' name='music' value='Salsa'> Salsa <br>
    <input type='checkbox' name='music' value='Pop'> Pop <br>
    <input type='checkbox' name='music' value='Metal'> Metal <br>
    <input type='checkbox' name='music' value='Reggae'> Reggae <br>
    <input type='checkbox' name='music' value='Alternativo'> Alternativo <br>
    <input type='checkbox' name='music' value='Electrónica'> Electrónica <br>
    <input type='checkbox' name='music' value='Regional'> Regional <br>
    <input type='checkbox' name='music' value='Cubana'> Cubana <br>
    <input type='checkbox' name='music' value='Vallenato'> Vallenato <br>
    <input type='checkbox' name='music' value='Reggeton'> Reggeton <br>
    <input type='checkbox' name='music' value='Otro'> Otro <br>
    <h3>¿Qué tipo de espacios te gustan?</h3>
    <input type='checkbox' name='spaces' value='Abiertos'> Abiertos <br>
    <input type='checkbox' name='spaces' value='Cerrados'> Cerrados <br>
    <input type='checkbox' name='spaces' value='Libre'> Al aire libre <br>
    <h3>¿En qué zona de la ciudad vives? </h3>
    <input type='radio' name='zona' value='Norte'> Norte <br>
    <input type='radio' name='zona' value='Sur'> Sur <br>
    <input type='radio' name='zona' value='Centro'> Centro <br>
    <input type='radio' name='zona' value='Este'> Este <br>
    <input type='radio' name='zona' value='Oeste'> Oeste <br>
    <h3>¿Qué tipo de ambientación te gusta mas?</h3>
    <input type='checkbox' name='ambient' value='Clasico'> Clásico <br>
    <input type='checkbox' name='ambient ' value='Moderno '> Moderno  <br>
    <input type='checkbox' name='ambient' value='Gotico'> Gótico <br>
    <input type='checkbox' name='ambient' value='Metalero'> Metalero <br>
    <br><input type='submit' id='enviar'>
    </form>
    `;
    var that = this;
    formulario.querySelector('form').addEventListener('submit', (e) => {
      e.preventDefault();
      var elemsF = formulario.querySelectorAll('[name=food]');
      var elemsM = formulario.querySelectorAll('[name=music]');
      var elemsA = formulario.querySelectorAll('[name=ambient]');
      var elemsS = formulario.querySelectorAll('[name=spaces]');
      var food = [];
      var music = [];
      var ambient = [];
      var spaces = [];

      for (var i = 0; i < elemsF.length; i++) {
        if (elemsF[i].checked) {
          food.push(elemsF[i].value);
        };
      };

      for (var i = 0; i < elemsM.length; i++) {
        if (elemsM[i].checked) {
          music.push(elemsM[i].value);
        };
      };

      for (var i = 0; i < elemsA.length; i++) {
        if (elemsA[i].checked) {
          ambient.push(elemsA[i].value);
        };
      };

      for (var i = 0; i < elemsS.length; i++) {
        if (elemsS[i].checked) {
          spaces.push(elemsS[i].value);
        };
      };
      that.onResults(that.miembro, that.miGrupo, e.target.presupuesto.value, e.target.isFood.value, food, music, spaces, e.target.zona.value, ambient, this.invitado);
    });
    return formulario;
  },

  render: function(pagina) {
    var container = document.getElementById('root');
    container.innerHTML = '';
    switch (pagina) {
      default: container.appendChild(this.getHome());
      break;
      case 'crear':
          container.appendChild(this.getCrearGrupo());
        break;

      case 'unir':
          container.appendChild(this.getUnirseGrupo());
        break;

      case 'grupo':
          container.appendChild(this.getMiGrupo());
        break;
      case 'formul':
          container.appendChild(this.getAllForm());
        break;
        /*
      case 'upload':
          container.appendChild(this.getUpload());
        break;*/
    }
  }
};
